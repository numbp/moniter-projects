package main

import (
	"time"

	"github.com/labstack/gommon/log"

	"github.com/pkg/errors"

	"fmt"
	"net/http"
	"os"

	"gitlab.com/k-terashima/utils/go-notify"

	"github.com/BurntSushi/toml"
)

type discord struct {
	ID        string
	Token     string
	ChannelID string
	Name      string
}

type project struct {
	ID   int
	URL  string
	Name string
}

type Projects struct {
	Discord discord
	Project []project
}

var (
	projects Projects
	dc       = notify.New(notify.TypeDiscord)
)

func init() {
	// setting output logs
	f, err := os.OpenFile("server.log", os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0644)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()
	log.SetOutput(f)

	// read setting file from toml
	if _, err := toml.DecodeFile("./projects.toml", &projects); err != nil {
		log.Fatal(err)
	}

	fmt.Printf("%+v\n", projects)
}

// This program gets management website status.
// Checks health for my works.
func main() {
	// uses all access
	client := new(http.Client)
	client.Timeout = time.Duration(10 * time.Second)
	dc = &notify.Discord{
		ID:        projects.Discord.ID,
		Token:     projects.Discord.Token,
		ChannelID: projects.Discord.ChannelID,
		Name:      projects.Discord.Name,
	}

	for {
		// repeat projects in toml
		for _, v := range projects.Project {
			go func(v project) {
				start := time.Now()
				defer func() {
					end := time.Now()
					fmt.Printf("処理時間 - %s: %v\n", v.Name, end.Sub(start))
				}()

				res, err := client.Get(v.URL)
				if err != nil {
					log.Error(err)
					dc.Set(err.Error())
					if err := dc.Send(); err != nil {
						log.Error(err)
					}

					return
				}
				if res == nil {
					log.Error("does not gets responce data")
					status := "not find"

					dc.Set(fmt.Sprintf("%s - %s", v.Name, status))
					if err := dc.Send(); err != nil {
						log.Error(err)
					}

					return
				}
				defer res.Body.Close()

				if res.StatusCode != 200 {
					dc.Set(fmt.Sprintf("%s - %s", v.Name, res.Status))
					if err := dc.Send(); err != nil {
						log.Error(err)
					}

					return
				}

				if time.Now().Sub(start) > time.Duration(9*time.Second) {
					dc.Set(errors.Wrapf(err, "%s - timeout, サーバー停止の可能性", v.Name))
					if err := dc.Send(); err != nil {
						log.Error(err)
					}

					return
				}

			}(v)
		}

		time.Sleep(2 * time.Hour)
	}

	err := errors.New("Attentions!! program stop!!")
	dc.Set(err)
	if err := dc.Send(); err != nil {
		log.Error(err)
	}
}
